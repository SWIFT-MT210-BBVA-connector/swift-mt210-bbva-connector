#BBVA-SWIFT


    MT740 - Sample letter of credit reference: sample sender NWBKTB2L - receiver: ROYCCUT2
    text :72Z:Please acknowledge receipt...

    MT210 - Sample receipt reference: BBBEBB0021CRESZZ - incoming payment
    curr: USD13000, receiver: Cred. Suiss: :52A: CRESSCHZZ71B

    MT720 - Sample auth to reimburse: sender: ECSESES - receiver MAHAUSS32
    text: :72Z:RESTRICTED TO CLAIMING BANK'S ACCEPTANCE COMMISSION

To connect to the jump server use **[RDP-CONNECT.PS1](https://gitlab.com/SWIFT-MT210-BBVA-connector/swift-mt210-bbva-connector/-/blob/main/RDP-CONNECT.PS1)** script.
